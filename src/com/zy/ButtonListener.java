package com.zy;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {
	private GamePanel pnlGame;
	public ButtonListener(GamePanel gp) {
		pnlGame = gp;
	}
	@Override
	public void actionPerformed(ActionEvent ae) {
		int x;
		if("左移".equals(ae.getActionCommand())) {
			x = pnlGame.getTx() - 3;
			//pnlGame.getX()
			pnlGame.setTx(x);			
			
		}else if("右移".equals(ae.getActionCommand())) {
			x = pnlGame.getTx() + 3;
			pnlGame.setTx(x);
		}
		pnlGame.repaint();
		System.out.println("我被按下了：---"+ae.getActionCommand());
	}

}
