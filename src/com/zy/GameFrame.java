package com.zy;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.zy.test.Car;
import com.zy.test.Train;

public class GameFrame extends JFrame{

	public void initFrame() {
		this.setBounds(new Rectangle(800, 600));
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("第一个窗口");
				
		//创建按钮对象 
		JButton btnOK1 = new JButton("开始游戏");
		JButton btnOK2 = new JButton("暂停游戏");
		JButton btnOK3 = new JButton("关于游戏");
		JButton btnOK4 = new JButton("左移");
		JButton btnOK5 = new JButton("右移");
		//创建JPanel
		JPanel pnlCenter = new GamePanel();
		JPanel pnlRight = new JPanel();
		JPanel pnlTop = new JPanel();
		JPanel pnlBottom = new JPanel();
		//设置边框
		pnlCenter.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		pnlRight.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		pnlTop.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		pnlBottom.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		pnlRight.setPreferredSize(new Dimension(100,0));
		pnlTop.setPreferredSize(new Dimension(0,30));
		
		//设置区域
		Container cnt = this.getContentPane();
		cnt.setLayout(new BorderLayout()); //BorderLayout

		cnt.add(pnlTop,BorderLayout.NORTH);
		cnt.add(pnlBottom,BorderLayout.SOUTH);
		cnt.add(pnlRight,BorderLayout.EAST);
		cnt.add(pnlCenter,BorderLayout.CENTER);
		//pnlCenter.add(btnOK);
		//JLabel lbl = new JLabel("");
		//在右边的区域里放置按钮，默认布局管理是FlowLayout
		pnlRight.add(btnOK1);
		pnlRight.add(btnOK2);
		pnlRight.add(btnOK3);
		FlowLayout fl = (FlowLayout)pnlRight.getLayout();
		fl.setAlignment(FlowLayout.CENTER);
		fl.setVgap(30);
		
		//pnlTop.add(btnOK4);
		//pnlTop.add(btnOK5);
		//ButtonListener lsnButton = new ButtonListener((GamePanel)pnlCenter);
		//btnOK4.addActionListener(lsnButton);
		//btnOK5.addActionListener(lsnButton);
		pnlCenter.setFocusable(true);
		PlayerInputListener input = new PlayerInputListener((GamePanel)pnlCenter);
		pnlCenter.addKeyListener(input);
		//显示窗体
		this.setVisible(true);

	}

	public static void main(String[] args) {
		//构造窗体对象
		new GameFrame().initFrame();
		//GameFrame gf = new GameFrame();
		//gf.initFrame();
		//frmMain.setBounds(x, y, width, height);

		
//		Car car1 = new Train();	
//		car1.run();	//train running...
//
//		Car car2 = new Car();
//		car2.run(); //car running...
//
//		//car1 = car2;
//		//car1.run();  //car running...
//		Train t1 = (Train)car1;
//		t1.load();
//
//		Train t2 = (Train) car2;
//		t2.run();
//		//t2.load();
		
		
	}

}
