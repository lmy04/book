package com.zy;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class GamePanel extends JPanel {
	private Image imgTank;
	public int tx = 0;
	public int ty = 100;
	
	public GamePanel() {
		String strPath = this.getClass().getResource("/").getPath();
		System.out.println(strPath);
		imgTank = new ImageIcon(strPath+"images/m4-3-1.png").getImage();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		//g.drawString("���", 50, 50);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.drawImage(imgTank, tx,ty,this);
		
		System.out.println("painting...."+imgTank.getWidth(this));		
	}

	public int getTx() {
		return tx;
	}

	public void setTx(int tx) {
		this.tx = tx;
	}

	public int getTy() {
		return ty;
	}

	public void setTy(int ty) {
		this.ty = ty;
	}


	
	
}
