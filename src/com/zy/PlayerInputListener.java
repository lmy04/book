package com.zy;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PlayerInputListener implements KeyListener {
	private GamePanel pnlGame;
	public PlayerInputListener(GamePanel gp) {
		pnlGame = gp;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int x=0;
		if(e.getKeyCode()==KeyEvent.VK_LEFT) {
			x = pnlGame.getTx() - 3;
			//pnlGame.getX()
			pnlGame.setTx(x);			
			System.out.println("LEFT KEY");
		}else if(e.getKeyCode()==KeyEvent.VK_RIGHT) {
			x = pnlGame.getTx() + 3;
			pnlGame.setTx(x);			
			System.out.println("RIGHT KEY");
		}
		
		pnlGame.repaint();

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
